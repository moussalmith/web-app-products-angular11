import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Product} from '../model/product.model';

@Injectable({providedIn: 'root'})
export class ProductsService {
  host = environment.host;
  constructor(private http: HttpClient) {
  }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.host + '/products');
  }

  getAllProductSelected(): Observable<Product[]> {
    return this.http.get<Product[]>(this.host + '/products?selected=true');
  }

  deleteProduct(product: Product): Observable<void>{
    product.selected = !product.selected;
    return this.http.delete<void>(this.host + '/products/' + product.id);
  }

  getAvailableProduct(): Observable<Product[]> {
    return this.http.get<Product[]>(this.host + '/products?available=true');
  }

  select(product: Product): Observable<Product>{
    product.selected = !product.selected;
    return this.http.put<Product>(this.host + '/products/' + product.id, product);
  }

  getSelectedProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(this.host + '/products?selected=true');
  }
  getAvailableProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(this.host + '/products?available=true');
  }

  save(product: Product) {
    return this.http.post<Product>(this.host + '/products', product);
  }

  getProduct(id: number): Observable<Product>{
    return this.http.get<Product>(this.host + '/products/' + id);
  }
  updateProduct(product: Product): Observable<Product>{
    return this.http.put<Product>(this.host + '/products/' + product.id, product);
  }

  searchProducts(keyword: string): Observable<Product[]>{
    return this.http.get<Product[]>(this.host + '/products?name_like=' + keyword);
  }
}
